import React from 'react'

function InputField({ label = null, className = "", onChange = null, type = "text", help = null, value = null, required = true }) {
  return (
    <div class={`form-group ${className}`}>
      {label && (
        <label>{label}</label>
      )}
      <input type={type} className="form-control" value={value} onChange={onChange} required={required} />
      {help && <small id="emailHelp" class="form-text text-muted">{help}</small>}
    </div>
  )
}

export default InputField

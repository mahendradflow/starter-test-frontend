import React from 'react';
import { Redirect, Switch, Route, NavLink } from 'react-router-dom';

import Home from './containers/Home/Home';
import Pricing from './containers/Pricing/Pricing';
import Checkout from './containers/Checkout/Checkout';
import Profile from './containers/Profile/Profile';

function App() {
  return (
    <React.Fragment>
      <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
        <h5 class="my-0 mr-md-auto font-weight-normal">Frontend Test</h5>
        <nav class="my-2 my-md-0 mr-md-3">
          <NavLink class="p-2 text-dark" to="/">Home</NavLink>
          <NavLink class="p-2 text-dark" to="/pricing">Pricing</NavLink>
          <NavLink class="p-2 text-dark" to="/profile">Profile</NavLink>
          <NavLink class="p-2 text-dark" to="/checkout">Checkout</NavLink>
        </nav>
      </div>

      <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
        <h1 class="display-4">Welcome to Frontend Test</h1>
      </div>

      <div class="container">
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/pricing" exact component={Pricing} />
          <Route path="/checkout" component={Checkout} />
          <Route path="/profile"exact component={Profile} />
          {/* <Route path="/user"exact component={User} /> */}
          <Redirect to="/" />
        </Switch>
      </div>
    </React.Fragment>
  );
}

export default App;

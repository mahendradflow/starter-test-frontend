import React from 'react';

export const UserProfileContext = React.createContext({
  firstname: {
    value: "",
    onChange: null,
  },
  lastname: {
    value: "",
    onChange: null,
  },
  phoneNumber: {
    value: "",
    onChange: null,
  },
  address1: {
    value: "",
    onChange: null,
  },
  address2: {
    value: "",
    onChange: null,
  },
});
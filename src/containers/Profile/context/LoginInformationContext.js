import React from 'react';

export const LoginInformationContext = React.createContext({
  username: {
    value: "",
    onChange: null
  },
  password: {
    value: "",
    onChange: null
  },
});
import React, { useContext } from 'react'
import InputField from '../../../../components/Form/InputField/InputField'
import { UserProfileContext } from '../../context/UserProfileContext'

function UserProfile() {
  const profileContext = useContext(UserProfileContext)

  return (
    <div className="col-12 col-lg-6">
      <h2>User Profile</h2>
      <InputField label="First Name" />
      <InputField label="Last Name"s />
      <InputField label="Phone Number" />
      <InputField label="Address Line 1" />
      <InputField label="Address Line 2" />
    </div>
  )
}

export default UserProfile

import React from 'react'
import InputField from '../../../../components/Form/InputField/InputField'

function LoginInformation() {
  return (
    <div className="col-12 col-lg-6">
      <h2>Login Information</h2>
      <InputField label="Email Address" type="email" />
      <InputField label="Password" type="password" />
    </div>
  )
}

export default LoginInformation;

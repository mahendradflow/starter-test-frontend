import React, { useState, useRef } from 'react'

import LoginInformation from './components/LoginInformation/LoginInformation'
import UserProfile from './components/UserProfile/UserProfile'
import { UserProfileContext } from './context/UserProfileContext'
import { LoginInformationContext } from './context/LoginInformationContext'

function Profile() {
  const formRef = useRef();
  const [firstname, setFirstname] = useState('');

  const validateForm = () => {
    const form = formRef.current;
    let isValid = true;

    isValid = form.checkValidity();
    form.classList.add('was-validate');

    return isValid;
  };

  const handleSave = () => {
    if(validateForm()) {
      // save to localstorage
    }

    return "happy code";
  }

  const userProfileContextValue = {
    firstname: {
      value: "",
      onChange: null,
    }
  };

  return (
    <div className="container">
      <form className="needs-validation" novalidate ref={formRef}>
        <div className="row">
          <UserProfileContext.Provider>
            <UserProfile />
          </UserProfileContext.Provider>
          <LoginInformationContext.Provider>
            <LoginInformation />
          </LoginInformationContext.Provider>
        </div>
        <button type="button" className="btn btn-primary" onClick={handleSave}>Save</button>
      </form>
    </div>
  )
}

export default Profile;

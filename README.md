# Starter Test Frontend

## Introduction
Please fork this git to your bitbucket. \
If you don't have bitbucket account, please create first. This test will cover:
- scss
- react with es6
- html
- bootstrap
- git

This test focus on frontend, any backend need will use dummy online data.

There are several task will be focus on:
- Bugfixing
- Refactoring code (make good quality of code)
- Feature
- Styling scss

Please create branch for each task, and once done, merge into master so it will be have history on your branch master.

## Bugfix (difficulty low)
- Crash app first running
- routing checkout but showing home
- jsx warning & error on console log
- if you find anything, please fix it make sure console error clean.

## Refactoring (difficulty medium)
- home page card refactoring with loop component. create new Presentational component for card
- refactor checkout page, how to make label and input live together (hint: design pattern)

## Feature (difficulty high)
* On /profile page, please add function to save data
* Please create new page with route "/users".
* load all user from jsonplaceholder (https://jsonplaceholder.typicode.com/users) and show it in users page.
* Use any libraries that you prefer to help you with any underlying functionality (axios, rxjs, fetch, react-redux, suspense, anything). There's no limitation in what libraries you may use for this.
* Quality before speed: Although time/speed/efficiency should be considered, quality of the code is way more important. If you can't finish all of the tasks, focus on what you've already accomplished.

## Styling (difficulty low)
Please update boostrap scss:
- font opensans https://fonts.google.com/specimen/Open+Sans 
- root font size 13px
- change bgcolor palette with all text color #fff
  - info: #5578eb
  - primary: #5867dd
  - danger: #fd397a
  - warning: #ffb822
  - secondary: #e1e1ef
  - success: #1dc9b7
- heading color #646c9a \
  size:
  - h1 32.5px
  - h2 26px
  - h3 22.75px
  - h4 19.5px
  - h5 16.25px
  - h6 13px
- change all border radius 10 (input, card, etc)